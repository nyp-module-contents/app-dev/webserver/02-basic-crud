"""
The routes module contains standalone examples
"""
import flask
import utils.flask
import json

#	This is a child route, so we don't specify url_prefix like the previous one
from data.products import Product, Products

router = flask.Blueprint("products", __name__)

@router.get("/")
def page_root():
	return flask.redirect(flask.url_for(".page_list"))
@router.get("/list")
def page_list():
	return flask.render_template("admin/products/list.html", data=Products.list())
@router.get("/create")
def page_create():
	return flask.render_template("admin/products/create.html", data= {})

@router.get("/update/<id>")
def page_update(id:str):
	p = Products.select(id)
	return flask.render_template("admin/products/update.html", id=id, data=p)

@router.get("/retrieve")
def api_retrieve():
	data    = Products.list()
	convert = list(map(lambda d: { "name":  d.name, "price": str(d.price), "description": d.description }, data))

	offset  = int(flask.request.args.get("offset", "0"))
	limit   = int(flask.request.args.get("limit", "0"))
	sort    = flask.request.args.get("sort",  "name")
	order   = flask.request.args.get("order", "asc") == "asc"
	search  = flask.request.args.get("search", None)
	filters = json.loads(flask.request.args.get("filter", "{}"))

	def test(x):
		a = ((filters["name"]        in x["name"])  if "name" in filters else True)
		b = ((filters["price"]       in x["price"]) if "price" in filters else True)
		c = ((filters["description"] in x["description"]) if "description" in filters else True)
		return a and b and c

	if sort is not None:
		convert = sorted(convert, key=lambda x: x[sort], reverse= not order)
	if len(filters) > 0:
		convert = list(filter(test, convert))

	return json.dumps({
		"total": len(convert),
		"rows" : convert[offset: offset + limit]
	})

@router.post("/create")
def api_create():
	logger = utils.flask.logger()
	# for k in flask.request.form.keys():
	# 	logger.info(f"Key: {k} Value: {flask.request.form.get(k, 'null')}")
	p = Product()
	p.name        = flask.request.form.get("name")
	p.price       = flask.request.form.get("price")
	p.description = flask.request.form.get("description")
	Products.insert(p)
	return flask.redirect(flask.url_for(".page_root"))

@router.post("/update/<id>")
def api_update(id: str):
	logger = utils.flask.logger()
	for k in flask.request.form.keys():
		logger.info(f"Key: {k} Value: {flask.request.form.get(k, 'null')}")
	data = flask.request.form.to_dict()
	logger.debug(f"Incoming data: {data}")

	p             = Products.select(id)
	p.name        = flask.request.form.get("name")
	p.price       = flask.request.form.get("price")
	p.description = flask.request.form.get("description")

	#	Replacement
	Products.delete(p.uuid)
	Products.insert(p)

	return flask.render_template("admin/products/update.html", id=id, data=data)


@router.get("/delete/<id>")
def api_delete(id: str):
	logger = utils.flask.logger()
	logger.warn(f"Deleted {id}")
	Products.delete(id)
	return flask.redirect(flask.url_for(".page_root"))