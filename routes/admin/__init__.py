"""
The routes module contains standalone examples
"""
import flask
import routes.admin.products as RProducts
#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("admin", __name__)
router.register_blueprint(RProducts.router, url_prefix="/products")
